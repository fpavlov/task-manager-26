package ru.t1.fpavlov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IProjectTaskService {

    void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    );

    void removeProject(
            @Nullable final String userId,
            @Nullable final Project project
    );

    void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    );

    void unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    );

}
