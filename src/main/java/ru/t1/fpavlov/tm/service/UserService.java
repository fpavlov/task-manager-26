package ru.t1.fpavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.repository.IUserRepository;
import ru.t1.fpavlov.tm.api.service.IPropertyService;
import ru.t1.fpavlov.tm.api.service.IUserService;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.exception.entity.UserNotFoundException;
import ru.t1.fpavlov.tm.exception.field.EmailEmptyException;
import ru.t1.fpavlov.tm.exception.field.EmailExistException;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.PasswordEmptyException;
import ru.t1.fpavlov.tm.exception.user.LoginEmptyException;
import ru.t1.fpavlov.tm.exception.user.LoginExistException;
import ru.t1.fpavlov.tm.exception.user.RoleEmptyException;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.HashUtil;

/**
 * Created by fpavlov on 20.12.2021.
 */
public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserRepository repository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository
    ) {
        super(repository);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (this.isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return this.repository.create(login, HashUtil.salt(this.propertyService, password));
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (this.isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (this.isEmailExist(email)) throw new EmailExistException();
        return this.repository.create(login, HashUtil.salt(this.propertyService, password), email);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (this.isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        return this.repository.create(login, HashUtil.salt(this.propertyService, password), role);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return this.repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return this.repository.findByEmail(email);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = this.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        this.taskRepository.clear(user.getId());
        this.projectRepository.clear(user.getId());
        return this.remove(user);
    }

    @Nullable
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = this.findByEmail(email);
        return this.remove(user);
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = this.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(this.propertyService, password));
        return user;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = this.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Nullable
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return this.repository.isLoginExist(login);
    }

    @Nullable
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return this.repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = this.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setIsLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = this.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setIsLocked(false);
    }

}
