package ru.t1.fpavlov.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 06.12.2021.
 */
@NoArgsConstructor
public abstract class AbstractException extends RuntimeException {

    @NotNull
    public AbstractException(@Nullable final String message) {
        super(message);
    }

    @NotNull
    public AbstractException(
            @Nullable final String message,
            @Nullable final Throwable cause
    ) {
        super(message, cause);
    }

    @NotNull
    public AbstractException(@Nullable final Throwable cause) {
        super(cause);
    }

    @NotNull
    public AbstractException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
