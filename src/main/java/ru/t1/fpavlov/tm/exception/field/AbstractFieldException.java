package ru.t1.fpavlov.tm.exception.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.exception.AbstractException;

/**
 * Created by fpavlov on 06.12.2021.
 */
@NoArgsConstructor
public class AbstractFieldException extends AbstractException {

    @NotNull
    public AbstractFieldException(@Nullable final String message) {
        super(message);
    }

    @NotNull
    public AbstractFieldException(
            @Nullable final String message,
            @Nullable final Throwable cause
    ) {
        super(message, cause);
    }

    @NotNull
    public AbstractFieldException(@Nullable final Throwable cause) {
        super(cause);
    }

    @NotNull
    public AbstractFieldException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
