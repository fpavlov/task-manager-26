package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Registry a new user in the system";

    @NotNull
    public static final String NAME = "registry";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String login = this.input("Login:");
        @NotNull final String password = this.input("Password:");
        @NotNull final String email = this.input("E-mail:");
        @NotNull final User user = this.getAuthService().registry(login, password, email);
        this.renderUser(user);
    }

}
