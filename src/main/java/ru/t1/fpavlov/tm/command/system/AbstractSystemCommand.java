package ru.t1.fpavlov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.service.ICommandService;
import ru.t1.fpavlov.tm.api.service.IPropertyService;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 07.12.2021.
 */
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return this.serviceLocator.getCommandService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return this.getServiceLocator().getPropertyService();
    }

}
